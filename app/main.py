from __future__ import absolute_import, print_function

import os, sys

from flask import Flask

ACCESS = {
    'guest': 0,
    'user': 1,
    'admin': 2
}

mongo_uri = ''

def create_app(config_filename=None): 
    app = Flask(__name__)

    app.config.from_pyfile('config.py')

    global mongo_uri
    mongo_uri = app.config['MONGO_URI']

    from .model import bcrypt 
    bcrypt.init_app(app)

    from .model import jwt
    jwt.init_app(app)

    from .model import cors
    cors.init_app(app, resources={
        r"/*":{
        "origins": "*"
        }
    })

    from .http.api.admin import admin
    from .http.api.test import test
    from .http.api.submission import submission
    
    app.register_blueprint(admin)
    app.register_blueprint(test)
    app.register_blueprint(submission)

    return app 