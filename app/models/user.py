from marshmallow import Schema, fields, post_load
from ..main import ACCESS
from ..model import bcrypt
from bson import ObjectId
from flask_jwt_extended import create_access_token, decode_token

Schema.TYPE_MAPPING[ObjectId] = fields.Str()


class User():
    def __init__(self, name, username, password_hash="dummy", access=ACCESS['user']):
        self.name = name
        self.username = username
        self.password_hash = password_hash
        self.access = access
    
    def is_admin(self):
        return self.access == ACCESS['admin']

    def hash_password(self, password):
        self.password_hash = bcrypt.generate_password_hash(password, 10)

    def verify_password(self, password):
        return bcrypt.check_password_hash(self.password_hash, password)

    def generate_auth_token(self):
        return create_access_token(identity={
            'username': self.username, 
            'password_hash': self.password_hash,
            'access': self.access
        })

    @staticmethod
    def verify_auth_token(token):
        return decode_token(token)

    @staticmethod
    def generate_hash_password(password):
        return str(bcrypt.generate_password_hash(password, 10), 'utf-8')


class UserSchema(Schema):
    id = fields.String()
    username = fields.Str(required=True)
    password_hash = fields.Str(required=True)
    name = fields.Str(required=True)
    access = fields.Int()

    @post_load
    def make_user(self, data, **kwargs):
        return User(**data)