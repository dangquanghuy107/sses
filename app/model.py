import os 
from flask import current_app

from pymongo import MongoClient
from .main import mongo_uri
mongo = MongoClient(mongo_uri).db


from flask_bcrypt import Bcrypt
bcrypt = Bcrypt()


from flask_jwt_extended import JWTManager
jwt = JWTManager()


from flask_cors import CORS
cors = CORS()