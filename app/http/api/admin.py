from flask import request, jsonify, Blueprint
from bson.json_util import dumps
import json 
from ...models.user import UserSchema, User
from ...model import mongo


admin = Blueprint('admin', __name__)


@admin.route('/admin/login', methods=["POST"])
def login():
    user_collection = mongo.users
    username = request.get_json()['username']
    password = request.get_json()['password']

    if username is None or password is None:
        return jsonify({"error": "Username or password is missing"})
    if len(username) == 0 or len(password) == 0:
        return jsonify({"error": "Username or password is missing"})

    result = ""

    user_data = user_collection.find_one({'username': username})

    if user_data:
        dictionary = json.loads(dumps(user_data))
        del dictionary['_id']
        user_schema = UserSchema()
        user_result = user_schema.load(dictionary)
        if user_result.verify_password(password):
            access_token = user_result.generate_auth_token()
            result = jsonify({"token": access_token})
        else:
            result = jsonify({"error": "Wrong username or password"})
    else:
        result = jsonify({"error": "Wrong username or password"})   

    return result



@admin.route('/admin/change_password', methods=["POST"])
def change_password():
    user_collection = mongo.users

    username = 'admin'

    password = request.get_json()['password']
    new_password = request.get_json()['new_password']
    new_password_retype = request.get_json()['new_password_retype']

    if password is None:
        return jsonify({"error": "Password required"})
    if new_password is None or len(new_password) < 8:
        return jsonify({"error": "Password length must be at least 8"})
    if new_password_retype is None or new_password_retype != new_password:
        return jsonify({"error": "Retyped password doesn't match"})
    
    user_data = user_collection.find_one({'username': username})

    if user_data:
        dictionary = json.loads(dumps(user_data))
        del dictionary['_id']
        user_schema = UserSchema()
        user = user_schema.load(dictionary)

        if not user.verify_password(password):
            return jsonify({"error": "Wrong password"})

        new_password_hash = User.generate_hash_password(new_password)

        user_collection.find_one_and_update({'username': username}, {'$set': {'password_hash': new_password_hash}})
        return jsonify({"success": "Password changed"})
    
    return jsonify({"error": "Cannot change password"})


@admin.route('/register', methods=["POST"])
def register():
    user_collection = mongo.users

    username = request.get_json()['username']
    password = request.get_json()['password']
    name = request.get_json()['name']

    if username is None or len(username) == 0:
        return jsonify({"error": "Username required"})
    if password is None:
        return jsonify({"error": "Password required"})
    if len(password) < 8:   
        return jsonify({"error": "Password length must be at least 8"})
    if name is None or len(name) == 0:
        return jsonify({"error": "Name required"})

    result = ""

    user_data = user_collection.find_one({'username': username})
    if user_data:
        result = jsonify({"error": "Username existed"})
    else:
        user = User(name, username)
        user.hash_password(password)
        user_schema = UserSchema()
        dictionary = user_schema.dump(user)
        user_collection.insert_one(dictionary)
        result = jsonify({"success": "user registered"})

    return result