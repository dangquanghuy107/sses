from flask import Blueprint, request, jsonify, current_app

from ...model import mongo 
from .test import db

from random import seed, randint


submission = Blueprint('submission', __name__)
seed(1)


def generate_permutation(num):
    result = [] 
    for _ in range(num):
        while True:
            x = randint(0, num - 1)
            if not x in result:
                result.append(x)
                break

    return result


@submission.route("/ping", methods=["GET"])
def ping():
    return jsonify(generate_permutation(4))


@submission.route('/submission', methods=["POST"])
def get_submission_info():
    user_email = request.get_json()['email']
    test_id = request.get_json()['test_name']
    test_type = request.get_json()['test_type']

    submission_collection = mongo.submissions

    query = submission_collection.find_one({
        'email': user_email, 
        'test_name': test_id
    })

    if test_type not in test_id:
        return jsonify({"error": "Invalid Test ID"})

    if query: 
        if query['is_finished']:
            return jsonify({"error": "You have already finished this test"})
        return jsonify({
            'message': 'success',
            'permutation': query['permutation'],
            'test_name': test_id,
            'last_index': query['last_index'],
            'test_result': query['test_result']
        })
    else:
        test_collection = mongo.tests

        test_query = test_collection.find_one({'test_name': test_id})
        
        if not test_query:
            return jsonify({"error": "Test not found"})

        test_sets = test_query['test_sets']

        test_result = []

        score = 0
        if test_type == 'MOS': 
            score = 3
        elif test_type == 'MUSHRA':
            score = 50
        else:
            score = '...'

        for i in range(len(test_sets)):
            if test_type == 'INT':
                test_result.append([score, 3])
            else: 
                test_result.append([score for j in range(len(test_sets[i]['files']))])

        permutation = []

        for i in range(len(test_sets)):
            permutation.append(generate_permutation(len(test_sets[i]['files'])))

        submission_collection.insert_one({
            'email': user_email,
            'test_name': test_id,
            'permutation': permutation,
            'last_index': 0,
            'test_result': test_result,
            'is_finished': False
        })

        return jsonify({
            'message': 'success',
            'test_name': test_id,
            'permutation': permutation,
            'last_index': 0,
            'test_result': test_result
        })


@submission.route('/update_result', methods=["POST"])
def update_result():
    email = request.get_json()['email']
    test_name = request.get_json()['test_name']
    new_result = request.get_json()['new_result']
    last_index = request.get_json()['last_index']
    is_finished = request.get_json()['is_finished']

    submission_collection = mongo.submissions 

    query = submission_collection.find_one({'email': email, 'test_name': test_name})

    if query:
        result = query['test_result']
        if last_index > len(result) - 1:
            return jsonify({"error": "invalid index"})
        result[last_index] = new_result
        submission_collection.find_one_and_update({'email': email, 'test_name': test_name}, {
            '$set': {
                'test_result': result,
                'last_index': last_index,
                'is_finished': is_finished
            }
        })
    else:
        return jsonify({"error": "submission not found"})

    return jsonify({"message": "success"})