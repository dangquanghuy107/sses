from flask import request, jsonify, Blueprint, Response, send_file, current_app
from werkzeug.utils import secure_filename
from bson.json_util import dumps
from ...model import mongo

import zipfile
import uuid

import os, sys, shutil
import openpyxl
from openpyxl.utils import get_column_letter


test_type_set = ["MOS", "INT", "MUSHRA"]

test = Blueprint('test', __name__)


def db(string):
    print(string, file=sys.stderr)


def get_cell(sheet, coordinate):
    return sheet[coordinate].value


def parse_int(filepath, test_name):
    wb = openpyxl.load_workbook(filepath)
    utterances = wb['Utterances']

    result = {} 
    result['_id'] = uuid.uuid4()
    result['test_name'] = test_name 
    result['num_tests'] = 0
    result['test_sets'] = [] 
    result['test_type'] = 'INT'

    max_row = utterances.max_row 
    result['requirement_1'] = get_cell(utterances, 'A1')

    for row in range(3, max_row + 1): 
        senID = get_cell(utterances, 'B' + str(row))
        filename = get_cell(utterances, 'C' + str(row))
        result['num_tests'] += 1
        result['test_sets'].append({
            'name': senID,
            'files': [filename],
            'text': get_cell(utterances, 'D' + str(row))
        })

    parameters = wb['Parameters']
    result['requirement_2'] = get_cell(parameters, 'B1')
    result['score_label'] = [] 
    for row in range(2, parameters.max_row + 1):
        result['score_label'].append(get_cell(parameters, 'B' + str(row)))

    tests_collection = mongo.tests 
    tests_collection.insert_one(result)

    return jsonify(result)



def parse_mos(filepath, test_name):
    wb = openpyxl.load_workbook(filepath)
    utterances = wb['Utterances']

    result = {}
    result['_id'] = uuid.uuid4()
    result['test_name'] = test_name 
    result['num_tests'] = 0
    result['test_sets'] = []
    result['test_type'] = 'MOS'

    max_row = utterances.max_row

    for row in range(2, max_row + 1): 
        senID = get_cell(utterances, 'B' + str(row))
        filename = get_cell(utterances, 'C' + str(row))
        result['num_tests'] += 1
        result['test_sets'].append({
            'name': senID, 
            'files': [filename],
        })
    
    parameters = wb['Parameters']
    result['requirement'] = get_cell(parameters, 'B1')
    result['score_label'] = []
    for row in range(2, parameters.max_row + 1): 
        result['score_label'].append(get_cell(parameters, 'B' + str(row)))

    test_collection = mongo.tests 
    test_collection.insert_one(result)

    return jsonify(result)


# increasing SenID, increasing group id
def parse_mushra(filepath, test_name):
    wb = openpyxl.load_workbook(filepath)
    utterances = wb['Utterances']

    result = {} 
    result['_id'] = uuid.uuid4()
    result['test_name'] = test_name 
    result['num_tests'] = 0
    result['test_sets'] = []
    result['test_type'] = 'MUSHRA'
    
    max_row = utterances.max_row 

    def parse_mushra_row(row):
        if get_cell(utterances, 'C' + str(row)) == 'reference':
            result['test_sets'][-1]['reference'] = get_cell(utterances, 'D' + str(row))
        elif get_cell(utterances, 'C' + str(row)) == 'text':
            result['test_sets'][-1]['text'] = get_cell(utterances, 'D' + str(row))
        else:
            result['test_sets'][-1]['files'].append(get_cell(utterances, 'D' + str(row)))

    for i in range(1, max_row):
        last_senID = get_cell(utterances, 'B' + str(i))
        cur_senID = get_cell(utterances, 'B' + str(i + 1))
        if cur_senID != last_senID:
            result['num_tests'] += 1
            result['test_sets'].append({
                "name": cur_senID, 
                "files": [],
                "reference": ""
                })
        parse_mushra_row(i + 1)

    parameters = wb['Parameters']
    result['requirement'] = get_cell(parameters, 'B1')
    result["score_label"] = []

    for i in range(2, parameters.max_row + 1):
        result["score_label"].append(get_cell(parameters, 'B' + str(i)))

    test_collection = mongo.tests 
    test_collection.insert_one(result)

    return jsonify(result)


def parse_test(filename):
    path = os.path.join(current_app.config['UPLOAD_FOLDER'], filename.split('.')[0])

    def get_test_type(path):
        for _root, _dirs, files in os.walk(path):
            for f in files:
                for test_type in test_type_set:
                    if test_type in f:
                        return test_type
            break 
    
    test_type = get_test_type(path)

    if test_type == 'MUSHRA':
        return parse_mushra(os.path.join(path, "MUSHRA.xlsx"), filename.split('.')[0])
    elif test_type == 'MOS':
        return parse_mos(os.path.join(path, "MOS.xlsx"), filename.split('.')[0])
    elif test_type == 'INT':
        return parse_int(os.path.join(path, "INT.xlsx"), filename.split('.')[0])


@test.route('/test/upload', methods=["POST"])
def file_upload():
    if 'file' not in request.files:
        return jsonify({"error": "No file uploaded!"})
    file = request.files['file']
    
    file_ext = file.filename.split('.')[-1]

    if file_ext != 'zip':
        return jsonify({"error": "You must upload a zip file!"})
    
    filename = secure_filename(file.filename)

    import os, sys
    
    for _root, _dirs, files in os.walk(current_app.config['UPLOAD_FOLDER']):
        for name in files:
            if filename == name:
                return jsonify({"error": "Duplicated filename!"})

    test_type = ''
    for _type in test_type_set:
        if _type in filename:
            test_type = _type 
    
    if test_type == '':
        return jsonify({"error": "Invalid filename"})
    
    file.save(os.path.join(current_app.config['UPLOAD_FOLDER'], filename))

    #extract the zip file
    archive = zipfile.ZipFile(file)

    extract_folder_path = os.path.join(current_app.config['UPLOAD_FOLDER'], file.filename.split('.')[0])

    archive.extractall(path=extract_folder_path)

    #get test data
    parse_test(file.filename.split('.')[0])


    return jsonify({"message": "File uploaded!"})


@test.route('/test/delete/<filename>', methods=["DELETE"])
def delete_test(filename):
    found = False 
    for _root, _dirs, files in os.walk(current_app.config['UPLOAD_FOLDER']):
        for name in files:
            if filename in name:
                found = True

    if not found:
        return jsonify({"error": "File not found"})
    
    os.remove(os.path.join(current_app.config['UPLOAD_FOLDER'], filename + '.zip'))
    shutil.rmtree(os.path.join(current_app.config['UPLOAD_FOLDER'], filename))

    submissions_collection = mongo.submissions 
    submissions_collection.delete_many({'test_name': filename})

    tests_collection = mongo.tests 
    tests_collection.delete_many({'test_name': filename})

    return jsonify({"message": "success"})


@test.route('/test', methods=["GET"])
def get_test_list():
    test_list = []
    for _root, _dirs, files in os.walk(current_app.config['UPLOAD_FOLDER'], topdown=False):
        for filename in files:
            if filename.split('.')[-1] == 'zip':
                test_list.append(filename.split('.')[0])

    return jsonify(test_list)


@test.route('/test/<testname>', methods=["GET"])
def get_test(testname):
    test_collection = mongo.tests
    
    query = test_collection.find_one({'test_name': testname})

    if not query: 
        return jsonify({"error": "Test not found."})

    return query


@test.route('/test/download/<filename>', methods=["GET"])
def download_test(filename):
    return parse_test(filename)
    # return os.path.join('uploads', filename.split('.')[0])
    # return send_file(os.path.join('uploads', filename))


@test.route('/get_audio/<test_name>/<audio_name>', methods=["GET"])
def get_audio(test_name, audio_name):
    return send_file(os.path.join('uploads', test_name, 'wav', audio_name), mimetype="audio/wav", cache_timeout=0)


@test.route('/test/export_result/<test_name>', methods=["GET"])
def export_result(test_name):
    path = os.path.join(current_app.config['UPLOAD_FOLDER'], test_name)
    is_int_test = ('INT' in test_name)

    src = ""

    for filename in os.listdir(path):
        if filename.split('.')[-1] == 'xlsx': # file extension must be xlsx
            src = os.path.join(path, filename)
            dst = os.path.join(path, test_name + '_result.xlsx')
    
    if not src: 
        return jsonify({"error": "Cannot find xlsx file"})

    # Insert result into file
    wb = openpyxl.open(src)
    
    if 'Listeners' in wb.sheetnames: 
        lis = wb.get_sheet_by_name('Listeners')
        wb.remove_sheet(lis)
    if 'Results' in wb.sheetnames:
        res = wb.get_sheet_by_name('Results')
        wb.remove_sheet(res)
    if 'Results_level' in wb.sheetnames:
        res_level = wb.get_sheet_by_name('Results_level')
        wb.remove_sheet(res_level)

    def insert_listener():
        listener_sheet = wb.create_sheet('Listeners')
        listener_sheet['A1'] = 'No'
        listener_sheet['B1'] = 'Email'
        listener_sheet.column_dimensions['B'].width = 50

        submissions_collection = mongo.submissions 
        query = submissions_collection.find({'test_name': test_name})
        
        row_index = 1
        for submission in query: 
            row_index += 1
            listener_sheet['A' + str(row_index)] = row_index - 1
            listener_sheet['B' + str(row_index)] = submission['email']

    
    def insert_result():
        utterance_sheet = wb['Utterances']
        result_sheet = wb.create_sheet('Results')
        result_sheet.column_dimensions['D'].width = 20
        
        for i in range(1, utterance_sheet.max_column + 1):
            result_sheet[get_column_letter(i) + '1'] = utterance_sheet[get_column_letter(i) + '1'].value
                
        # delete reference row in MUSHRA test
        def copy_utterance(dst_sheet):
            row_index = 1
            for r in range(2, utterance_sheet.max_row + 1):
                invalid_row = False 
                for c in range(1, utterance_sheet.max_column + 1): 
                    if get_cell(utterance_sheet, get_column_letter(c) + str(r)) == "reference":
                        invalid_row = True
                    if get_cell(utterance_sheet, get_column_letter(c) + str(r)) == "text":
                        invalid_row = True
                if not invalid_row:
                    if get_cell(utterance_sheet, 'A' + str(r)) != 'No':
                        row_index += 1
                        dst_sheet['A' + str(row_index)] = row_index - 1
                        for col in range(2, utterance_sheet.max_column + 1):
                            dst_sheet[get_column_letter(col) + str(row_index)] = get_cell(utterance_sheet, get_column_letter(col) + str(r))
                    else: 
                        dst_sheet['A' + str(row_index)] = 'No'
                        for col in range(2, utterance_sheet.max_column + 1):
                            dst_sheet[get_column_letter(col) + str(row_index)] = get_cell(utterance_sheet, get_column_letter(col) + str(r))

            
            if is_int_test: 
                dst_sheet.delete_cols(dst_sheet.max_column, 1) 

        copy_utterance(result_sheet)
        if is_int_test:
            result_sheet_level = wb.create_sheet('Results_level')
            copy_utterance(result_sheet_level)

        max_col = result_sheet.max_column

        submissions_collection = mongo.submissions 
        query = submissions_collection.find({'test_name': test_name, 'is_finished': True})

        for submission in query:
            max_col += 1
            result_sheet.column_dimensions[get_column_letter(max_col)].width = 20
            result_sheet[get_column_letter(max_col) + '1'] = submission['email']
            if is_int_test:
                result_sheet_level[get_column_letter(max_col) + '1'] = submission['email']

            test_result = []
            test_result_level = []
            
            for result in submission['test_result']:
                if is_int_test:
                    test_result += [result[0]]
                    test_result_level += [result[1]]
                else:
                    test_result += result 

            for i in range(0, len(test_result)):
                result_sheet[get_column_letter(max_col) + str(i + 2)] = test_result[i]
            
            if is_int_test: 
                for i in range(0, len(test_result_level)):
                    result_sheet_level[get_column_letter(max_col) + str(i + 2)] = test_result_level[i]


    insert_listener()
    insert_result()
    # Save into result file
    wb.save(dst)

    return send_file(os.path.join('uploads', test_name, test_name + '_result.xlsx'),
                    as_attachment=True,
                    cache_timeout=-1)