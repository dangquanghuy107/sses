# SSES - Synthesized speech evaluation system

Hệ thống phục vụ đánh giá chất lượng tiếng nói tổng hợp

## Project setup

- Backend

```
docker-compose build
docker-compose up
```

- Frontend 
```
docker build --build-arg VUE_APP_API_URL={your api url goes here} . -t sses-frontend:production
docker run -p 80:80 -d sses-frontend:production
```

Trong đó, API_URL là `http:// + IP + :5000`

VD: 

```
docker build --build-arg VUE_APP_API_URL=http://192.168.55.104:5000 . -t sses-frontend:production
docker run -p 80:80 -d sses-frontend:production
```

## Endpoints
- `/dashboard`: Quản lí test data và test results. Cần login (username: "admin", password: "danghuy09").
- `/mos`: MOS test
- `/int`: Intelligibility test 
- `/mushra`: MUSHRA test

## Upload bài test 

### Quy tắc chung: *Lưu ý:* các yêu cầu chính xác (Case-sensitive) sẽ được **in đậm**
- Testdata để dưới định dạng **zip**, tên file zip có dạng `MMDDYYYY_(MOS/INT/MUSHRA)_(Số thứ tự).zip`, tên file zip này cũng là tên của bài test.
- Trong file zip gồm 1 file **xlsx**, tên file có dạng `(MOS/INT/MUSHRA).xlsx`, và một folder tên **wav** chứa tất cả file audio. 
  
### MOS (Mean Opinion Score) test:

- File `MOS.xlsx` gồm 2 sheets: **Utterances** và **Parameters**.
- Sheet Utterances: 
  - `No` : số thứ tự file audio 
  - `ID` : ID file, cũng là filename 
  - `FileName`: tên file audio 

<center>

| No | ID    | FileName  |
|----|-------|-----------|
| 1  | Sen1  | Sen1.wav  |
| 2  | Sen2  | Sen2.wav  |
| 3  | Sen3  | Sen3.wav  |
| 4  | Sen4  | Sen4.wav  |
| 5  | Sen5  | Sen5.wav  |
| 6  | Sen6  | Sen6.wav  |
| 7  | Sen7  | Sen7.wav  |
| 8  | Sen8  | Sen8.wav  |
| 9  | Sen9  | Sen9.wav  |
| 10 | Sen10 | Sen10.wav |
</center>

- Sheet Parameters: Text và mô tả các thang điểm

<center>

| Text | Đánh giá độ tự nhiên của câu nói nghe được |
|------|--------------------------------------------|
| 1    | Tồi - Rất không tự nhiên                   |
| 2    | Kém - Không tự nhiên                       |
| 3    | Trung bình - Chấp nhận được                |
| 4    | Khá - Khá tự nhiên                         |
| 5    | Tốt - Rất tự nhiên                         |
</center>

### Intelligibility test:

- File `INT.xlsx` gồm 2 sheets: **Utterances** và **Parameters**.
- Sheet Utterances: 
  - Ô `A1` : Text yêu cầu/hướng dẫn thực hiện cho người nghe để fill vào test interface
  - `No` : số thứ tự file audio 
  - `ID` : ID file, cũng là filename 
  - `FileName`: tên file audio 
  - *Lưu ý*: Ở phần text ta sẽ sử dụng kí tự *Ellipsis* (…) thay cho 3 dấu chấm.
  
<center> 

| Nghe và điền từ nghe được được vào dấu …  |       |           |                                     |
|-------------------------------------------|-------|-----------|-------------------------------------|
| No                                        | ID    | FileName  | Text                                |
| 1                                         | Sen1  | Sen1.wav  | Bạn sẽ nghe thấy từ … tiếp theo đây |
| 2                                         | Sen2  | Sen2.wav  | Bạn sẽ nghe thấy từ … tiếp theo đây |
| 3                                         | Sen3  | Sen3.wav  | Bạn sẽ nghe thấy từ … tiếp theo đây |
| 4                                         | Sen4  | Sen4.wav  | Bạn sẽ nghe thấy từ … tiếp theo đây |
| 5                                         | Sen5  | Sen5.wav  | Bạn sẽ nghe thấy từ … tiếp theo đây |
| 6                                         | Sen6  | Sen6.wav  | Bạn sẽ nghe thấy từ … tiếp theo đây |
| 7                                         | Sen7  | Sen7.wav  | Bạn sẽ nghe thấy từ … tiếp theo đây |
| 8                                         | Sen8  | Sen8.wav  | Bạn sẽ nghe thấy từ … tiếp theo đây |
| 9                                         | Sen9  | Sen9.wav  | Bạn sẽ nghe thấy từ … tiếp theo đây |
| 10                                        | Sen10 | Sen10.wav | Bạn sẽ nghe thấy từ … tiếp theo đây |

</center>

- Sheet Parameters: 

<center>

| Text | Đánh giá độ dễ nghe của từ cần điền |
|------|-------------------------------------|
| 1    | Tồi - Không thể nghe được           |
| 2    | Kém - Rất khó nghe                  |
| 3    | Trung bình - Chấp nhận được         |
| 4    | Khá - Khá dễ nghe                   |
| 5    | Tốt - Rất dễ nghe                   |
</center>

### MUSHRA test:

- File `MUSHRA.xlsx` gồm 2 sheets: **Utterances** và **Parameters**.
- Sheet Utterances: 
  - `No` : số thứ tự file audio 
  - `SenID`: Câu nói có cùng 1 nội dung 
  - `Group`: ID của model hoặc của hệ thống TTS, có giá trị **reference** nếu đó là file reference, **text** nếu đó là expected text. 
  - `FileName`: tên file audio 

- *Lưu ý:* Các file có chung SenID (cả reference) phải được đặt liên tiếp nhau.

<center>

| No | SenID | Group     | FileName           |
|----|-------|-----------|--------------------|
| 1  | Sen1  | 1         | Sen1_1.wav         |
| 2  | Sen1  | 2         | Sen1_2.wav         |
| 3  | Sen1  | 3         | Sen1_3.wav         |
| 4  | Sen1  | 4         | Sen1_4.wav         |
| 5  | Sen1  | reference | Sen1_reference.wav |
| 6  | Sen2  | 1         | Sen2_1.wav         |
| 7  | Sen2  | 2         | Sen2_2.wav         |
| 8  | Sen2  | 3         | Sen2_3.wav         |
| 9  | Sen2  | 4         | Sen2_4.wav         |
</center>

- Sheet Parameters: 
  
<center> 

| Text | Đánh giá độ tự nhiên của câu nói nghe được |
|------|--------------------------------------------|
| 1    | Tồi - Rất không tự nhiên                   |
| 25   | Kém - Không tự nhiên                       |
| 50   | Trung bình - Chấp nhận được                |
| 75   | Khá - Khá tự nhiên                         |
| 100  | Tốt - Rất tự nhiên                         |
</center>

