FROM python:3.6-slim as build

ENV STATIC_URL /static
ENV STATIC_PATH /var/www/app/static

WORKDIR /var/www/

RUN python -m venv /opt/venv

ENV PATH="/opt/venv/bin:$PATH"

RUN apt-get update && apt-get install -y --no-install-recommends build-essential gcc

COPY requirements.txt .

RUN pip install -r requirements.txt

RUN pip install gunicorn

FROM python:3.6-slim AS run

COPY --from=build /opt/venv /opt/venv

COPY . .

ENV PATH="/opt/venv/bin:$PATH"

CMD gunicorn -b :5000 --access-logfile - --error-logfile - wsgi:app